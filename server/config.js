var config = {
    local: {
        mode: 'local',
        port: 8080,
        dbMongo: 'mongodb://localhost:27017/dodge_and_shoot',
        socket: {url:'http://192.168.1.116',port:9000},
        secret:'secr22_sahj34uq89ncds',
        sessionMaxAge: 36000000,
        game: {
          width: 1000,
          height: 1000,
          maxPlayers: 10,
          maxHeartbeatInterval: 5000,
          player:{
//            базовая скорость игрока
            speed: 15,
//            базовые жизни игрока
            life: 10,
//            радиус игпрока
            radius: 40,
            speedRotation: 0.1
          },
          bullet:{
//          базовая скорость пуль
            speed: 40,
//            базвый урон пуль
            power: 1,
//            время жизни пули (колиечество расчетов  - calcDo)
            life: 100
          },
//          расчетов в секунду (попадания-движеиня)
          calcDo: 60,
//          отправка данных игрокам в секунду
          updateFactor: 5,
        }
    },
    production: {
        mode: 'production',
        port: 8080,
        dbMongo: 'mongodb://localhost:27017/dodge_and_shoot',
        socket: {url:'http://192.168.1.116',port:9000},
        secret:'secr22_sahj34uq89ncds',
        sessionMaxAge: 36000000,
        game: {
          width: 5000,
          height: 5000,
          maxPlayers: 10,
        },
        network:{
          updateFactor: 40,
        }
    }
}
module.exports = function(mode) {
    return config[mode || process.argv[2] || 'local'] || config.local;
}
