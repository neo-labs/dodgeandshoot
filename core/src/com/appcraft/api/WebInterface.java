package com.appcraft.api;


import com.appcraft.response.PlayerInfoResponse;
import com.appcraft.response.RatingResponse;
import com.appcraft.utils.Constants;

import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Query;

public interface WebInterface {

    @GET(Constants.API_PLAYER_INFO)
    void getPlayerInfo(@Query("player_id") String playerId, WebCallback<PlayerInfoResponse> callback);

    @GET(Constants.API_RATING)
    void getRating(WebCallback<RatingResponse> callback);

    @FormUrlEncoded
    @POST(Constants.API_BUY_SKIN)
    void buySkin(@Field("player_id") String playerId, @Field("skin_id") String skinId, WebCallback<ServerResponse> callback);

}