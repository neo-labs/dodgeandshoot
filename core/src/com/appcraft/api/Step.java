package com.appcraft.api;

public class Step {
    private float x;
    private float y;
    private float a;

    public Step(float x, float y, float a) {
        this.x = x;
        this.y = y;
        this.a = a;
    }

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }

    public float getA() {
        return a;
    }
}
