package com.appcraft.utils;

public interface IPurchaseFinished {
    void onIabPurchaseFinished(boolean result, String sku);
}
