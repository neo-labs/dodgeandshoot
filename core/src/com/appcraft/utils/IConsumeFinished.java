package com.appcraft.utils;

public interface IConsumeFinished {
    void onIabConsumeFinished(boolean result, String sku);
}
