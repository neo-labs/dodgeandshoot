package com.appcraft.response;

import com.appcraft.api.ServerResponse;
import com.appcraft.model.PlayerInfo;

public class PlayerInfoResponse extends ServerResponse {
    public PlayerInfo data;
}
